﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;

public class CameraController : NetworkBehaviour
{
    [SerializeField] private Transform playerCameraTransform = null;
    [SerializeField] private float speed = 20f;
    //падинг для краев экрана (верхней, нижней, левой, правой сторон),
    // чтобы при наведении в эту область (почти в края экрана, у нас было движение, т.е как в рпг или стратегиях)
    [SerializeField] private float screenBorderThickness = 10f;
    [SerializeField] private Vector2 screenXLimits = Vector2.zero;
    [SerializeField] private Vector2 screenZLimits = Vector2.zero;

    private Vector2 previousInput; // потому что вы выбрали при настроке scheme Vector2

    private Controls controls;

    public override void OnStartAuthority()
    {
        playerCameraTransform.gameObject.SetActive(true);

        controls = new Controls();

        //Эти события можно не отписывать, автор не нашел нигде в документации
        controls.Player.MoveCamera.performed += SetPreviousInput;
        controls.Player.MoveCamera.canceled += SetPreviousInput;

        controls.Enable(); //просто обязаны вызвать
    }

    [ClientCallback]
    private void Update()
    {
        //!hasAuthority - чтобы не двигать камеры всех остальных клиентов. isFocused - если игра свернута
        if (!hasAuthority || !Application.isFocused) { return; }

        UpdateCameraPosition();
    }

    private void UpdateCameraPosition()
    {
        Vector3 pos = playerCameraTransform.position;

        //Если не нажали ничего на клавиатуре (из WASD наших), т..е говорится, если в previousInput не пришло движение (т.е оно равно нулю)
        if (previousInput == Vector2.zero)
        {
            Vector3 cursorMovement = Vector3.zero;

            Vector2 cursorPosition = Mouse.current.position.ReadValue();

            if (cursorPosition.y >= Screen.height - screenBorderThickness)
            {
                cursorMovement.z += 1;
            }
            else if (cursorPosition.y <= screenBorderThickness)
            {
                cursorMovement.z -= 1;
            }

            if (cursorPosition.x >= Screen.width - screenBorderThickness)
            {
                cursorMovement.x += 1;
            }
            else if (cursorPosition.x <= screenBorderThickness)
            {
                cursorMovement.x -= 1;
            }

            //Делаем нормалайзд, чтобы была всегда одна скорость передвижения камеры,
            //потому что возврщает вектор длиной 1 (ну либо -1, если его значения были равны -1
            pos += cursorMovement.normalized * speed * Time.deltaTime;
        }
        //А если нажали на WASD
        else
        {
            //Здесь previousInput.y потому что previousInput - это Vector2
            pos += new Vector3(previousInput.x, 0f, previousInput.y) * speed * Time.deltaTime;
        }

        //Значения в скобках после pos - это просто граница от и до
        pos.x = Mathf.Clamp(pos.x, screenXLimits.x, screenXLimits.y);
        pos.z = Mathf.Clamp(pos.z, screenZLimits.x, screenZLimits.y);

        playerCameraTransform.position = pos;
    }

    private void SetPreviousInput(InputAction.CallbackContext ctx)
    {
        previousInput = ctx.ReadValue<Vector2>();
    }
}
