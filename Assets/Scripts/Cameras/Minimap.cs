﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;
using UnityEngine.EventSystems;

public class Minimap : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [SerializeField] private RectTransform minimapRect = null;
    [SerializeField] private float mapScale = 10f;
    [SerializeField] private float offset = -6f;

    private Transform playerCameraTransform;

    private void Update()
    {
        if (playerCameraTransform != null) { return; }

        //Если мы еще не подключились
        if (NetworkClient.connection.identity == null) { return; }

        playerCameraTransform = NetworkClient.connection.identity.GetComponent<RTSPlayer>().GetCameraTransform();
    }

    //При клике мыши. Если с помощью eventData не выбрать какой клик, будут работать все кнопки мыши (как минимум ЛКМ, ПКМ, и клик колесика)
    public void OnPointerDown(PointerEventData eventData)
    {
        MoveCamera();
    }

    //чтобы, когда двигали с зажатым курсором перемещались по миникарте камера тоже перемещалась
    public void OnDrag(PointerEventData eventData)
    {
        MoveCamera();
    }

    private void MoveCamera()
    {
        Vector2 mousePos = Mouse.current.position.ReadValue();

        //Если курсор не внутри миникарты, то выйти
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(minimapRect, mousePos, null, out Vector2 localPoint)) { return; }

        //Формула. Находим интерполяцию, что если при изменении размеров миникарты, все равно ловить курсор внутри этой миникарты.
        //Если этого не сделать, курсор будет ловиться только в изначально заданных размерах миникарты
        Vector2 lerp = new Vector2((localPoint.x - minimapRect.rect.x) / minimapRect.rect.width, 
                                  (localPoint.y - minimapRect.rect.y) / minimapRect.rect.height);

        Vector3 newCameraPos = new Vector3(
            Mathf.Lerp(-mapScale, mapScale, lerp.x), playerCameraTransform.position.y, Mathf.Lerp(-mapScale, mapScale, lerp.y));

        playerCameraTransform.position = newCameraPos + new Vector3(0f, 0f, offset);
    }   
}
