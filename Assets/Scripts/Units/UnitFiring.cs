﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitFiring : NetworkBehaviour
{
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private GameObject projectilePrefab = null;
    [SerializeField] private Transform projectileSpawnPoint = null;
    [SerializeField] private float fireRange = 5f;
    [SerializeField] private float fireRate = 1f; // сколько projectiles в присвоенное значение секунд спавнить
    [SerializeField] private float rotationSpeed = 20f;

    private float lastFireTime;

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.GetTarget();

        if (target == null) { return; }

        if (!CanFireAtTarget()) { return; }

        //Две строчки как сделать поворот танка на цель при стрельбе, если тот двигается
        Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position); //Вычисляет угол на который он должен повернуться до танка

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime); //Поворачивает объект с заданной rotation к конечной с определенной скоростью поворота, т.е от и до

        //Time.time - время со старта игры. Если оно больше 1, то каждую секунду будет запускаться код, если больше 0.5, то каждые 0.5 и т.д
        //тем самым, можно просто регулировать fireRate значение - это и будет цикл, это и будет задержка, равная одному и тому же числу
        //Таким образом можно сделать задержку стрельбы, задержку отлика юнитов голосом после каждого n-ого голоса и т.п.
        if (Time.time > (1 / fireRate + lastFireTime))
        {
            //Развернуть projectile в сторону таргета. То есть снаряд полетит из пушки прямо в цель, если пушка повернута к цели спиной
            Quaternion projectileRotation = Quaternion.LookRotation(target.GetAimAtPoint().position - projectileSpawnPoint.position);

            GameObject projectileInstance = Instantiate(projectilePrefab, projectileSpawnPoint.position, projectileRotation);

            NetworkServer.Spawn(projectileInstance, connectionToClient); //connectionToClient - говорит что это мы, этот класс, т.е тот кто стреляет - этот клиент

            lastFireTime = Time.time;
        }
    }

    //[Server], а не [ServerCallback] потому что это вызываем мы, а Update() по умолчанию сам пытается вызвать на клиентах и на сервере
    //И вообще, если что-то пишется в консоль с предупреждениями, надо смотреть на [Server] или [ServerCallback]
    [Server] 
    private bool CanFireAtTarget()
    {
        return (targeter.GetTarget().transform.position - transform.position).sqrMagnitude <= fireRange * fireRange;
    }
}
