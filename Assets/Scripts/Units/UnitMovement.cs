﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class UnitMovement : NetworkBehaviour
{
    [SerializeField] private NavMeshAgent agent = null;
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private float chaseRange = 10f;

    // private Camera mainCamera;

    #region Server

    public override void OnStartServer()
    {
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;
    }

    public override void OnStopServer()
    {
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }

    //Вопрос: Что отправляет здесь данные на сервер, если нет SyncVar переменной? Ответ? компонент NetworkTransform отправляет на север,
    //когда видит, что позиция (ротация, скейл )изменилась.

    //Здесь был сначала [Server], но это штука спамит в консоль что ты запускаешь на клиенте, хотя это сервер, а ServerCallback не кидает предупреждения
    //Применяем,когда мы не контролируем действия. 
    [ServerCallback] 
    private void Update()
    {
        Targetable target = targeter.GetTarget();

        if (target != null)
        {
            if ((target.transform.position - transform.position).sqrMagnitude > chaseRange * chaseRange)//Что-то типа Vector3.SqrMagnitude()
            {
                agent.SetDestination(target.transform.position);
            }
            else if (agent.hasPath)
            {
                agent.ResetPath();
            }

            return;
        }

        if (!agent.hasPath) { return; }

        if (agent.remainingDistance > agent.stoppingDistance) { return; }

        agent.ResetPath(); // Немедленно сбросить путь - остановить юнита
    }

    [Command] 
    public void CmdMove(Vector3 position)
    {
        ServerMove(position);
    }

    [Server]
    public void ServerMove(Vector3 position)
    {
        targeter.ClearTarget();

        //Смотрит, была ли переданная позиция (точка) в месте, где есть NavMesh, то есть покрыта ли местность NavMesh (голубым цветом)
        if (!NavMesh.SamplePosition(position, out NavMeshHit hit, 1f, NavMesh.AllAreas)) { return; }

        agent.SetDestination(hit.position);
    }

    [Server]
    private void ServerHandleGameOver()
    {
        agent.ResetPath();
    }

    #endregion

    //Это здесь было раньше

    //#region Client 

    //public override void OnStartAuthority() // Start() метод для клиента, кто отправляет действие, а не как Start для всех сразу на инициализации объекта
    //{
    //    mainCamera = Camera.main;
    //}

    //[ClientCallback]
    //private void Update()
    //{
    //    if (!hasAuthority) { return; } //убедиться, что это относится именно к тому, кто отправил действие. Если это не написать, будет пытаться выполнять код у всех остальных клиентов

    //    if (!Mouse.current.rightButton.wasPressedThisFrame) { return; } //Смотрит, нажата ли кнопка ПКМ.Если да, идет по коду дальше. Старый Input.GetMouseDown(1)

    //    Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()); //Это старый Input.mousePosition

    //    if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity)) { return; }// Если курсор находился над тем, на чем есть коллайдер, то идет дальше по коду. Т.е. если луч каснулся collider

    //    CmdMove(hit.point); //Передает точку хита
    //}

    //#endregion
}
