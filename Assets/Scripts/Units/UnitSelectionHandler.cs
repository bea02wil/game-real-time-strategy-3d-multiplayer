﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitSelectionHandler : MonoBehaviour
{
    [SerializeField] private RectTransform unitSelectionArea = null;

    [SerializeField] private LayerMask layerMask = new LayerMask();

    private Vector2 startPosition;
    
    private RTSPlayer player;
    private Camera mainCamera;

    public List<Unit> SelectedUnits { get; } = new List<Unit>();

    private void Start()
    {
        mainCamera = Camera.main;

        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        Unit.AuthorityOnUnitDespawned += AuthorityHandleUnitDespawned;
        GameOverHandler.ClientOnGameOver += ClientHandleGameOver;

        //identity - это компонент NetworkIdentity на player prefab. Т.е мы через этот объект на чем сидит UnitSelectionHandler
        //(UnitHandler), дальше через network соединение игрока обращаемся к скрипту Networkidentity и берем у него RTSPlayer.
        //То есть это просто ссылка на компонент
       // player = NetworkClient.connection.identity.GetComponent<RTSPlayer>(); 
    }

    private void OnDestroy()
    {
        Unit.AuthorityOnUnitDespawned -= AuthorityHandleUnitDespawned;
        GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
    }

    private void Update()
    {
        //Сюда перенесли пока, потому что таким способом избегаем ошибку пока не сделали лобби, когда идет обращение через Network,
        //но при загрузке сцены после меню коннекта еще не существует,а мы обращаемся к нетворку
        //Потом перенесли в Start(), когда появилось lobby
        //if (player == null)
        //{
        //    player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();
        //}

        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            StartSelectionArea();
        }
        else if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            ClearSelectionArea(); //Метод для выделения юнита спрайтом
        }

        else if (Mouse.current.leftButton.isPressed) //Если зажата ЛКМ
        {
            UpdateSelectionArea();
        }
    }

    private void StartSelectionArea()
    {
        if (!Keyboard.current.leftShiftKey.isPressed)
        {
            foreach (Unit selectedUnit in SelectedUnits)
            {
                selectedUnit.Deselect();
            }

            SelectedUnits.Clear();
        }     

        unitSelectionArea.gameObject.SetActive(true);

        startPosition = Mouse.current.position.ReadValue();

        UpdateSelectionArea();
    }

    private void UpdateSelectionArea()
    {
        Vector2 mousePosition = Mouse.current.position.ReadValue();

        //Находим ширину DragBox'а на каждом кадре. Если начертить в тетрадь и взять любой угол куда тянуть, то все понятно
        //тоже самое с высотой
        float areaWidth = mousePosition.x - startPosition.x;
        float areaHight = mousePosition.y - startPosition.y;

        //Ширина и высота должны быть всегда положительны. Опять же, если начертить, то будет все понятно. Наприм, если получить ширину из левого нижнего
        //угла в верхний левый, то ширина, допустим, -2 быть не может,потом что при сложении у нас фигура получится меньше, потому что в итоге мы будем отнимать
        //в sizeDelta кидаем полученный вектор, то есть разность между новыми размерами и старыми.
        unitSelectionArea.sizeDelta = new Vector2(Math.Abs(areaWidth), Mathf.Abs(areaHight));
        //Обновляем Pivot у фигуры. Если начертить, то все понятно.
        unitSelectionArea.anchoredPosition = startPosition + new Vector2(areaWidth / 2, areaHight / 2);
    }

    private void ClearSelectionArea()
    {
        unitSelectionArea.gameObject.SetActive(false);

        if (unitSelectionArea.sizeDelta.magnitude == 0)
        {
            Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask)) { return; }

            if (!hit.collider.TryGetComponent<Unit>(out Unit unit)) { return; } //если объект, который выделили не явл Танком (т.е. Unit скрипт не содержит)

            if (!unit.hasAuthority) { return; } //Можем выделять только на свои танки

            SelectedUnits.Add(unit);

            foreach (Unit selectedUnit in SelectedUnits)
            {
                selectedUnit.Select();
            }

            return;
        }

        //Я так понял не смотря на то что в инспекторе выбран bot left anchorPosition, то здесь считается от центра
        Vector2 min = unitSelectionArea.anchoredPosition - (unitSelectionArea.sizeDelta / 2); //находим минимум в прямоугольной форме выделения, т.е. нижнюю левую точку
        Vector2 max = unitSelectionArea.anchoredPosition + (unitSelectionArea.sizeDelta / 2); //находим максимум в прямоугольной форме выделения, т.е. верхнюю правую точку

        foreach (Unit unit in player.GetMyUnits())
        {
            if (SelectedUnits.Contains(unit)) { continue; } //чтобы избежать повторного добавления/выбора юнита при зажатом shift

            Vector3 screenPosition = mainCamera.WorldToScreenPoint(unit.transform.position);

            if (screenPosition.x > min.x && 
                screenPosition.x < max.x && 
                screenPosition.y > min.y && screenPosition.y < max.y)
            {
                SelectedUnits.Add(unit);
                unit.Select();
            }
        }
    }

    private void AuthorityHandleUnitDespawned(Unit unit)
    {
        SelectedUnits.Remove(unit);
    }

    private void ClientHandleGameOver(string winnerName)
    {
        //чтобы при окончании игры не работал метод Update(), т.е не могли ничего выделить
        enabled = false;
    }
}
